function diametroRueda(diametro) {
    if (diametro <= 10) {
        console.log("Es una rueda para un juguete pequeño");
    }else if ((diametro >10) && (diametro <20)) {
        console.log("Es una rueda para un juguete mediano");
    }else{
        console.log("Es una rueda para un juguete grande");
    }
}

// Tiene que salir "Es una rueda para un juguete pequeño"
diametroRueda(5);
console.log("----------------------------------");
//Tiene que salir "Es una rueda para un juguete mediano"
diametroRueda(15);
console.log("----------------------------------");
//Tiene que salir "Es una rueda para un juguete grande"
diametroRueda(25);